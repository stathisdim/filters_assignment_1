% Digital Filters Assignment 1
%
% author: Dimitriadis Efstathios, 8490
% email: efstathde@ece.auth.gr
% data: 4/4/2018

%% first part

R = [0.4852 -0.3785; -0.3785 0.4852]; % autocorrelation matrix computed bu hand
p = [0.19; 0]; % cross correlation vector computed by hand

w0 = R\p;

%% second part 

l = eig(R);
lamda_max = max(l);

m_max = 2/lamda_max;

%% third part
n = 1000;

w = [-1; -1]; 
mu = 1.1*m_max;

wt = zeros([2,n]);
wt(:,1) = w;

% s = [0; u];
for i=2:n
  w = w + mu*(p-R*w); % Adaptation steps
  wt(:,i) = w;
end


%% parameter error
figure(1)
we = (wt - w0*ones(1,n)).^2;
e = sqrt(sum(we));

semilogy(e);
xlabel('time step n');
ylabel('Parameter error');
title('Parameter error');


%% contour curves and trajectories
L = 50;
ww = linspace(-2.5,2.5,L);

J = zeros([L,L]);
sigma2d = 0.1;

% Construct the error surface
for i=1:L
  for k=1:L
    wp = [ww(i); ww(k)];
    J(k,i) = sigma2d - 2*p'*wp + wp'*R*wp;
  end
end

min_J = min(J(:));
max_J = max(J(:));

levels = linspace(min_J,max_J,12);

figure(2)
contour(ww, ww, J, levels); axis square
hold on

plot(wt(1,:), wt(2,:), 'xr--');
hold off
colorbar
xlabel('w(1)');
ylabel('w(2)');
title('Error Surface and Adaptation process');
