load("sounds.mat");

nCoeff = 500;
n = length(u);

% generate Toeplitz matrix
U = toeplitz( [u; zeros(nCoeff-1,1)], [u(1) zeros(1,nCoeff-1)] );

dPadded = [d; zeros(nCoeff-1,1)];

% compute auto-correlation and cross-correlation
R = U'*U / n;
p = U'*dPadded / n;

% compute R's eigenvalues 
l = eig(R);
lamda_max = max(l);

% calculate the optimal coefficients using Wiener-Hopf
w0 = R\p;

% compute the step for the deepest descent algorithm
mu = 0.9*(2/lamda_max);

w = rand(nCoeff, 1); 
y = zeros(n, 1);
wt = zeros(nCoeff, n);
wt(:,1) = w;

for i=nCoeff:n
    w = w + mu*(p-R*w); % Adaptation steps
    wt(:,i) = w;
    y(i) = u(i:-1:i-nCoeff+1)' * w; % filter
end

song = d-y;

soundsc(song,Fs);

